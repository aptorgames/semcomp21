if(instance_exists(target)){
	x_offset = camera_get_view_width(camera) / 2;
	y_offset = camera_get_view_height(camera) / 2;
	
	x = x + factor * (target.x - x);
	y = y + factor * (target.y - y);
	
	camera_set_view_pos(camera, x - x_offset, y - y_offset);
}