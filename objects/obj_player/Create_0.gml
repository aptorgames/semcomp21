grav = 4800; //px/s^2
pressed_left = false;
pressed_right = false;
move_direction = 0;
h_axis = 0;
pressed_jump = false;
jump_button_state = 0;
jump_velocity = -sqrt(2*grav*70);

wall_jump_v_velocity = jump_velocity ;
wall_jump_h_velocity = jump_velocity ;

hspd = 0;
vspd = 0;

on_ground = false;
parede_left = false;
parede_right = false;
pular = false;

enum pstates{
	idle,
	running,
	jump_impulse,
	wall_jump_impulse,
	wall_drag,
	on_air
}

state = pstates.idle;
prev_state = state;

move_accel = 8000;
max_h_spd = 400;
max_v_spd = 800;
frict= 6000; //px/s^2
grav_supress_factor = 0.4;
variable_jump = false;
can_ghost_jump = false;

ghost_jump_tolerance = .1;
buffer_jump_tolerance = .1;