{
    "id": "6ed8e638-6532-44bd-86f0-df185194fd6c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "da0e2bdf-fc72-4750-837e-72fe10d20e08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ed8e638-6532-44bd-86f0-df185194fd6c"
        },
        {
            "id": "d12f50a7-e378-429f-813b-c33436c76ac9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6ed8e638-6532-44bd-86f0-df185194fd6c"
        },
        {
            "id": "778b1643-b710-4f36-9b52-bb2bdbbf2489",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "6ed8e638-6532-44bd-86f0-df185194fd6c"
        },
        {
            "id": "f9764e5e-8299-47dd-9a03-36e7d2ab65b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6ed8e638-6532-44bd-86f0-df185194fd6c"
        },
        {
            "id": "d627a2e5-bf1c-4b9f-8929-443dd2ff7ad6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "6ed8e638-6532-44bd-86f0-df185194fd6c"
        }
    ],
    "maskSpriteId": "1ea44b85-512d-46d5-bba4-ed3a83fd79e6",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "1c5c3055-07bb-4666-9af4-e417b481c0ac",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "507a8365-bd4d-48c6-a55b-36b06aae82d5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 74,
            "y": 0
        },
        {
            "id": "e6689847-95c4-47f9-8c13-d1e4efcdd5df",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 74,
            "y": 70
        },
        {
            "id": "d98fa1d5-dbde-47d2-a33e-c6bb9c76e888",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 70
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1ea44b85-512d-46d5-bba4-ed3a83fd79e6",
    "visible": true
}