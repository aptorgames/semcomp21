#region Lendo inputs
h_axis = gamepad_axis_value(0, gp_axislh);
pressed_left = keyboard_check(vk_left) || (h_axis < -0.5) || keyboard_check(ord("A"));
pressed_right = keyboard_check(vk_right) || (h_axis > 0.5) || keyboard_check(ord("D"));
pressed_jump = keyboard_check_pressed(vk_space) ||	gamepad_button_check_pressed(0, gp_face1);
jump_button_state = keyboard_check(vk_space) || gamepad_button_check(0, gp_face1);
#endregion

#region Atualizando variáveis de troca de estado
move_direction = pressed_right - pressed_left;
on_ground = place_meeting(x, y + 1, obj_wall);
parede_left = place_meeting(x - 1, y, obj_wall);
parede_right = place_meeting(x + 1, y, obj_wall);
if(pressed_jump){
	pular = true;
	alarm[1] = room_speed * buffer_jump_tolerance;	
}
#endregion

#region Trocando estados
if(state == pstates.idle){
	if(pular && on_ground){
		state = pstates.jump_impulse;
	}
	else if( (pressed_right || pressed_left) && on_ground){
		state = pstates.running;
	}
	else if(!on_ground){
		state = pstates.on_air;
		can_ghost_jump = true;
		alarm[0] = room_speed * ghost_jump_tolerance;
	}
}
else if(state == pstates.running){
	if(pular && on_ground){
		state = pstates.jump_impulse;
	}
	else if( !pressed_right && !pressed_left && on_ground){
		state = pstates.idle;
	}
	else if(!on_ground){
		state = pstates.on_air;
		can_ghost_jump = true;
		alarm[0] = room_speed * ghost_jump_tolerance;
	}
}
else if(state == pstates.jump_impulse){
	state = pstates.on_air;
}
else if(state == pstates.wall_jump_impulse){
	state = pstates.on_air;
}
else if(state == pstates.on_air){
	if(pular && (parede_right || parede_left)){		
		state = pstates.wall_jump_impulse;
	}
	else if(can_ghost_jump && pular){
		state = pstates.jump_impulse;
	}
	else if(!pressed_right && !pressed_left && on_ground){
		state = pstates.idle;
	}
	else if( (pressed_right || pressed_left) && on_ground){
		state = pstates.running;
	}
	else if ( ( (parede_right && pressed_right) || (parede_left && pressed_left) ) && (vspd > 0) ){
		state = pstates.wall_drag;
	}
}
else if(state == pstates.wall_drag){
	if(pular){
		state = pstates.wall_jump_impulse;
	}
	if(on_ground && !pressed_left && !pressed_right){
		state = pstates.idle;
	}
	else if (on_ground && (pressed_left || pressed_right)){
		state = pstates.running;
	}
	else if( (!parede_left && !parede_right) || ( (parede_right && !pressed_right) || (parede_left && !pressed_left)) ){
		state = pstates.on_air;
	}
	
}
#endregion

#region Processando estados
switch(state){
	case pstates.idle:
		if(sprite_index != spr_idle){
			sprite_index = spr_idle;
			image_index = 0;
			image_speed = 1;
		}
		movi_horizontal();
		movi_vertical();
	break;
	
	case pstates.running:
		if(sprite_index != spr_running){
			sprite_index = spr_running;
			image_index = 0;
			image_speed = 1;
		}
		movi_horizontal();
		movi_vertical();
	break;
	
	case pstates.jump_impulse:
		vspd = jump_velocity;
		pular = false;
		variable_jump = true;
		can_ghost_jump = false;
	break;
	
	case pstates.wall_jump_impulse:
		vspd = wall_jump_v_velocity;
		hspd = image_xscale * wall_jump_h_velocity;
		pular = false;
		image_xscale = -image_xscale;
		//variable_jump = true;
		//can_ghost_jump = false;
	break;
	
	case pstates.on_air:
		if(sprite_index != spr_on_air){
			sprite_index = spr_on_air;
			image_index = 0;
			image_speed = 1;
		}
		//show_debug_message("Grounded: " + string(on_ground));
		movi_horizontal();
		
		if( (variable_jump && !jump_button_state) || (vspd > 0) ){
			variable_jump = false;
		}
		
		if(variable_jump){
			vspd = vspd + grav * grav_supress_factor * (1/60);
		}
		else{
			movi_vertical();
		}
	break;
	
	case pstates.wall_drag:
		if(sprite_index != spr_wall_drag){
			sprite_index = spr_wall_drag;
			image_index = 0;
			image_speed = 1;
		}
		hspd = 0;
		vspd = vspd + grav*0.3* (1/60);
		vspd = clamp(vspd, -max_v_spd*0.5,max_v_spd*0.5);
	break;
}
#endregion

#region Colisão e movimento
var x_displacement, y_displacement;

x_displacement = hspd * (1/60);
y_displacement = vspd * (1/60);

if(place_meeting(x + x_displacement, y, obj_wall)){
	while(!place_meeting(x + sign(x_displacement), y, obj_wall)){
		x = x + sign(x_displacement);
	}
	hspd = 0;
	x_displacement = 0;
}
x = x + x_displacement;

if(place_meeting(x, y + y_displacement, obj_wall)){
	while(!place_meeting(x, y+ sign(y_displacement), obj_wall)){
		y = y + sign(y_displacement);
	}
	vspd = 0;
	y_displacement = 0;
}
y = y + y_displacement;
#endregion