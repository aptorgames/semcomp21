{
    "id": "78d2e50b-a6f6-421e-9f21-c36080972ff2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dced44f4-8cb0-47f3-b64b-f64674702823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78d2e50b-a6f6-421e-9f21-c36080972ff2",
            "compositeImage": {
                "id": "c1ca3315-7937-4b6d-88ca-5f4cd41f94be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dced44f4-8cb0-47f3-b64b-f64674702823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25e1b0d0-2f66-42d6-90c5-76a41b85627e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dced44f4-8cb0-47f3-b64b-f64674702823",
                    "LayerId": "f24ef26d-de25-4d62-8928-ea1b3c1020c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f24ef26d-de25-4d62-8928-ea1b3c1020c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78d2e50b-a6f6-421e-9f21-c36080972ff2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}