{
    "id": "89805b75-a93e-48fd-9dcc-2cb50d2bd415",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_on_air",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 10,
    "bbox_right": 47,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02d6b100-664c-4248-8eee-2536b5715c21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89805b75-a93e-48fd-9dcc-2cb50d2bd415",
            "compositeImage": {
                "id": "608290b2-9dc1-4194-8013-44405eb45106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d6b100-664c-4248-8eee-2536b5715c21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73f1beb9-c3a2-4f8b-ba8e-198beb0052e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d6b100-664c-4248-8eee-2536b5715c21",
                    "LayerId": "ffb30051-f93a-40c1-8732-6aea711a139a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ffb30051-f93a-40c1-8732-6aea711a139a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89805b75-a93e-48fd-9dcc-2cb50d2bd415",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 95
}