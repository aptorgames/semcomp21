{
    "id": "1ea44b85-512d-46d5-bba4-ed3a83fd79e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 24,
    "bbox_right": 50,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc59692b-aa49-42fb-8715-cfaba91c64e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ea44b85-512d-46d5-bba4-ed3a83fd79e6",
            "compositeImage": {
                "id": "7a5f3d88-b8eb-4aec-8886-e347bf179c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc59692b-aa49-42fb-8715-cfaba91c64e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfe588f6-e9db-4ef7-88cc-b20e496b242f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc59692b-aa49-42fb-8715-cfaba91c64e4",
                    "LayerId": "1d57777f-9be5-4f8b-acde-a970083b39c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "1d57777f-9be5-4f8b-acde-a970083b39c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ea44b85-512d-46d5-bba4-ed3a83fd79e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 37,
    "yorig": 69
}