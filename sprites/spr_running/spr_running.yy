{
    "id": "a3306735-2c69-4ff6-9c28-69196ff95730",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 73,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a6a8dac-e9bb-4c88-bdaa-864c30d83b75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "1825073b-b43e-42a6-944b-de6023a08693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a6a8dac-e9bb-4c88-bdaa-864c30d83b75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf3d12c7-5ea7-4d18-a826-c9652b7f4749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a6a8dac-e9bb-4c88-bdaa-864c30d83b75",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        },
        {
            "id": "9e87f978-be06-4557-a958-813e9ae4fee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "44ad4223-8263-4e45-821a-18909813dbc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e87f978-be06-4557-a958-813e9ae4fee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c1602b9-761c-4246-b84a-59272eb2ba10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e87f978-be06-4557-a958-813e9ae4fee8",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        },
        {
            "id": "49510faa-9df0-4a20-81fa-ddbdc7991142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "0207323a-e63e-4743-a72f-15968a0f7fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49510faa-9df0-4a20-81fa-ddbdc7991142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ffcda67-4fc4-43a1-a591-4fe01aabaf50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49510faa-9df0-4a20-81fa-ddbdc7991142",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        },
        {
            "id": "d462ed5b-7132-4398-9263-b19b3763d466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "f4abd6b7-7f45-4001-8bf3-87af002875e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d462ed5b-7132-4398-9263-b19b3763d466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ac3d11d-333c-45df-b9a8-a95c9cef78f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d462ed5b-7132-4398-9263-b19b3763d466",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        },
        {
            "id": "66527b01-d3c0-40e7-a634-ce903231ac1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "842c9b56-7710-4064-9a01-18e2b03a7375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66527b01-d3c0-40e7-a634-ce903231ac1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7771ee33-40bd-4406-b057-9f994caa82a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66527b01-d3c0-40e7-a634-ce903231ac1a",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        },
        {
            "id": "2438d083-4fe6-47f0-bcaa-66a750feecd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "850bfffa-00e2-41e3-833e-78618fa8ee3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2438d083-4fe6-47f0-bcaa-66a750feecd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1e80949-2398-40d3-bce3-0d26b206925d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2438d083-4fe6-47f0-bcaa-66a750feecd3",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        },
        {
            "id": "86f3e81e-783e-4e82-834c-a3d4050dcd68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "75e43fe5-ef06-4439-9233-d38abea76a9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f3e81e-783e-4e82-834c-a3d4050dcd68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "425cfdb8-3645-4031-9763-a4f0a54de0c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f3e81e-783e-4e82-834c-a3d4050dcd68",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        },
        {
            "id": "6f3c2005-b48a-4f6c-9d6f-7297fcf6a955",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "1fe572d6-0d21-494f-92d0-6f1904f34eb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f3c2005-b48a-4f6c-9d6f-7297fcf6a955",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edbbcaf1-4a69-4718-99fb-49be857e08ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3c2005-b48a-4f6c-9d6f-7297fcf6a955",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        },
        {
            "id": "0787e779-4732-4500-92ff-f1a82eabe22d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "7411e175-59ca-410c-ab57-adf90035bf46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0787e779-4732-4500-92ff-f1a82eabe22d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a626cab3-b9b1-4662-aee5-3a9f31c078ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0787e779-4732-4500-92ff-f1a82eabe22d",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        },
        {
            "id": "44d0a357-0798-4be6-9d2d-66a84e2e79a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "compositeImage": {
                "id": "06672d5e-70f3-4fec-941b-15da73c003bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44d0a357-0798-4be6-9d2d-66a84e2e79a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5656d6e-9a13-4c1e-9100-3edb3a32cbf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44d0a357-0798-4be6-9d2d-66a84e2e79a0",
                    "LayerId": "24500043-883b-4850-956d-b487818fc494"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "24500043-883b-4850-956d-b487818fc494",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3306735-2c69-4ff6-9c28-69196ff95730",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 37,
    "yorig": 69
}