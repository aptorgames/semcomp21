{
    "id": "481c1bfe-6c8e-42a5-89e3-ff72d95ea2f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_drag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 5,
    "bbox_right": 60,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ce54de5-41b5-4feb-a73d-75c30986d9d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "481c1bfe-6c8e-42a5-89e3-ff72d95ea2f4",
            "compositeImage": {
                "id": "8f8c4f3c-bbb3-421d-96e0-9e5d94420b33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce54de5-41b5-4feb-a73d-75c30986d9d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c19df31-ee03-44d5-afe1-0980b1ca1087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce54de5-41b5-4feb-a73d-75c30986d9d8",
                    "LayerId": "7ba1c820-2fb6-41bb-90df-7b1680e4ab1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7ba1c820-2fb6-41bb-90df-7b1680e4ab1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "481c1bfe-6c8e-42a5-89e3-ff72d95ea2f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 95
}