{
    "id": "d7100728-4ec0-4d20-b9d9-e0cc79424011",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c36e2ca-ffd4-4ac7-a849-d482648fc209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7100728-4ec0-4d20-b9d9-e0cc79424011",
            "compositeImage": {
                "id": "344999ca-76f7-4911-abde-5d1a99463e2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c36e2ca-ffd4-4ac7-a849-d482648fc209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "253b285d-f761-4143-8f5b-f24b18c53375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c36e2ca-ffd4-4ac7-a849-d482648fc209",
                    "LayerId": "ad93e184-e320-4582-b3c9-a07c93de2ad8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "ad93e184-e320-4582-b3c9-a07c93de2ad8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7100728-4ec0-4d20-b9d9-e0cc79424011",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}