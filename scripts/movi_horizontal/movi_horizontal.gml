if(move_direction != 0){
	image_xscale = move_direction;
}
if(move_direction != 0){
	hspd = hspd + move_direction * move_accel * (1/60);
	hspd = clamp(hspd, -max_h_spd, max_h_spd);
}
else{
	var hspd_change = sign(hspd) * frict * (1/60);
	if(sign(hspd - hspd_change) == sign(hspd)){
		hspd = hspd - hspd_change;
	}
	else{
		hspd = 0;
	}
}